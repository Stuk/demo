module Tests

open Expecto

[<Tests>]
let tests =
    testList "Convert function" [
        testCase "Zero" <| fun _ ->
            Expect.equal "zero" (Program.convert 0) "Convert number 0"
        testCase "One" <| fun _ ->
            Expect.equal "one" (Program.convert 1) "Convert number 1"
        testCase "Several" <| fun _ ->
            Expect.equal "too many" (Program.convert 2) "Convert number 2"
    ]
    