﻿// For more information see https://aka.ms/fsharp-console-apps
printfn "Hello from F#"

let convert (n: int) : string =
    match n with
    | 0 -> "zero"
    | 1 -> "one"
    | _ -> "too many"
    